# Pizza Place

Sure you could figure out how to get [Dokku](http://dokku.viewdocs.io/dokku/)
to work, but this is much simpler: a quick & dirty way to automate the process
of creating an instance of your application based on a published branch,
triggered by way of a web hook.

There are two parts to this: the webhook listener (dispatcher) and the
instance-builder (kitchen).


## How it Works

Everything is tied in with your web hooks.  If you push a branch to GitLab,
and that branch isn't `master` or `develop`, PizzaPlace will spin up a fresh
instance of your application at `https://your-branch-name.yourdomain.tld`.  The
time between push and ready is mostly dependent on how heavy your application
is.

Sending additional pushes will blow away the old one and replace it with the
new one, complete with a fresh database every time.  You can even rebase or
reset and overwrite your entire branch history with `push -f` and PizzaPlace
will handle it just fine.


## Installation

As this is intended to be installed on a single-purpose cloud server, the installation is basically a shell script that does a bunch of things to your system to configure it to do the job.  Do not run this on your laptop:

```bash
# wget https://gitlab.com/danielquinn/pizzaplace/raw/master/install --output-document /install
# chmod 755 /install
# /install
```

The install script is interactive, asking you a bunch of questions, generating
keys and asking you to install said keys on your repo.  For the most part
though, it's pretty straightforward.


## Components


### Dispatcher

The dumb workhorse of the system, this is a crude Flask app that has one one
endpoint, `/dispatcher` which simply authenticates the request, decides whether
it's something we'll want to build an instance for, and if yes, creates a file
in the "add queue" directory.

The name of the file will be a URL-friendly name, and the contents of the file
will be the name of the branch in question.

The dispatcher runs as an unprivileged user: `pizzaplace`.


### Kitchen

Doing nearly all of the work, the kitchen is a much more complicated script,
but at its core the process is simple:

1. Look for a file in the `add` queue
2. If there's one there:
    1. Check out that branch into `/var/www/pizzaplace/NAME`
    2. Pick a random port number
    3. Create a Docker instance for this branch and have it listen on that port
    4. Create an Nginx config for it, pointing at our port
    5. Restart Nginx
3. GOTO 1.

As it modifies server configurations and restarts Nginx, `kitchen` must run as
**root**, however about 60% of the system calls it makes are done as the
`pizzaplace` user.


## Potentially Useful Information


### SSL

We manage all the SSL stuff with LetsEncrypt, which uses notoriously
short-lived certificates.  Unfortunately, since we're using a *wildcard
certificate*, the typical method of automatically updating your cert isn't
available without [considerable effort](https://certbot.eff.org/docs/using.html#pre-and-post-validation-hooks).

In the absence of that effort (say for example you're using a DNS provider
that doesn't have an API for changes), then you'll have to run this command
every 3 months or so.  Just remember to replace `${PARENT_DOMAIN}` with
whatever you specified above:

    certbot certonly --manual --preferred-challenges=dns -d "${PARENT_DOMAIN},*.${PARENT_DOMAIN}"


### Git

We speed up the process of checking out instances of the repo by using a git
reference directory at `/opt/git-reference`.  For the most part, you don't
need to worry about this, just don't delete this directory or Bad Things might
happen.


## Colophon

I was looking for a rough analogy for something that takes a job an spins up
new stuff according to instructions and a pizza place is what came to mind.

Credit for the adorable pizza icon goes to [u/CodexOfMaya](https://www.reddit.com/user/CodexOfMaya)
found [here](https://www.reddit.com/r/PixelArt/comments/5rtb15/ocnewbie_jumping_pizza/).

