# This can be any application server, not just Unicorn/Rainbows!
upstream pizzaplace_dispatcher {
  server unix:/var/run/pizzaplace/dispatcher.sock fail_timeout=0;
}


server {

  listen 80;
  listen [::]:80;
  server_name __PARENT_DOMAIN__;
  rewrite ^ https://$server_name$request_uri? permanent;

}

server {

  listen 443 ssl;
  listen [::]:443;
  client_max_body_size 4G;
  server_name __PARENT_DOMAIN__;
  keepalive_timeout 5;
  root /var/www/default;

  ssl_certificate /etc/letsencrypt/live/__PARENT_DOMAIN__/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/__PARENT_DOMAIN__/privkey.pem;
  ssl_trusted_certificate /etc/letsencrypt/live/__PARENT_DOMAIN__/fullchain.pem;
  ssl_session_timeout 1d;
  ssl_session_cache shared:SSL:50m;

  # Diffie-Hellman parameter for DHE ciphersuites, recommended 2048 bits
  # Generate with:
  #   openssl dhparam -out /etc/nginx/dhparam.pem 2048
  ssl_dhparam /etc/pki/nginx/dhparam.pem;

  # What Mozilla calls "Intermediate configuration"
  # Copied from https://mozilla.github.io/server-side-tls/ssl-config-generator/
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_ciphers 'ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS';
  ssl_prefer_server_ciphers on;

  add_header Strict-Transport-Security max-age=15768000;

  ssl_stapling on;
  ssl_stapling_verify on;

  location / {
    try_files $uri @proxy_to_app;
  }

  location @proxy_to_app {
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto https;
    proxy_set_header Host $host;
    proxy_redirect off;
    proxy_pass http://pizzaplace_dispatcher;
  }

}
