#!/usr/bin/env python3

#
# This is just a simple Flask-based web server that waits for a very specific
# POST request from GitLab consisting of push data.  It parses this request
# and writes a file to the `queue` directory with the name of the commit.
#
# A separate process, (kitchen) then monitors this directory to setup Nginx
# configs for each case, restarting the server as-needed.
#

import configparser
import logging
import os
import re
import sys

from flask import Flask, request

__ = configparser.RawConfigParser()
__.read("/etc/pizzaplace.conf")
config = dict(__["dispatcher"])

ADD_QUEUE = "/var/spool/pizzaplace/add"
DEL_QUEUE = "/var/spool/pizzaplace/remove"
SECRET = config["webhook_secret"]

application = Flask(__name__)

logging.basicConfig(level=config.get("LOG_LEVEL", "INFO"))
logger = logging.getLogger(__name__)


def parse_ref(ref):

    branch = re.sub(r"^refs/heads/", "", ref)
    name = re.sub(r"\W", "-", branch).strip("-").lower()

    return branch, name


@application.route("/")
def index():
    return """
        <html>
          <style>
            body {
              background-color: #666666;
            }
          </style>
          <body>
          </body>
        </html>
    """.replace("        ", "")


@application.route("/dispatcher", methods=("POST",))
def main():

    if "X-Gitlab-Token" not in request.headers:
        logger.error("No token found: %s", request.headers)
        return "1", 401, {}

    token = request.headers.get("X-Gitlab-Token")
    if not token == SECRET:
        logger.error("Token %s didn't match %s", token, SECRET)
        return "1", 401, {}

    if not request.is_json:
        logger.error("Request was not JSON")
        return "1", 400, {}

    post = request.get_json(force=True)

    logger.info("POST: %s", post)

    if not post["object_kind"] == "push":
        logger.warning("Request was not a push")
        return "1", 200, {}

    branch, name = parse_ref(post["ref"])

    queue = ADD_QUEUE
    if post["after"] == "0000000000000000000000000000000000000000":
        if post["checkout_sha"] is None:
            queue = DEL_QUEUE

    with open(os.path.join(queue, name), "w") as f:
        f.write(str(branch))

    return "0", 201, {}


def startup_checks():

    if not ADD_QUEUE or not DEL_QUEUE:
        sys.exit("Queues aren't defined! Exiting")

    if not SECRET:
        sys.exit("Secret key isn't defined! Exiting")

    if not os.path.exists(ADD_QUEUE):
        sys.exit(
            "The add queue doesn't exist.\n"
            "Did you forget to start up the kitchen first?"
        )


if __name__ == "__main__":
    startup_checks()
    application.run()
